// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDeBLl7WaXdZvQ3rJElFL1CwFAiXmQbr8Q",
    authDomain: "whatsapp-da4a5.firebaseapp.com",
    databaseURL: "https://whatsapp-da4a5.firebaseio.com",
    projectId: "whatsapp-da4a5",
    storageBucket: "whatsapp-da4a5.appspot.com",
    messagingSenderId: "1090147119245",
    appId: "1:1090147119245:web:bb6be8e20bb0798e30b2c4"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
