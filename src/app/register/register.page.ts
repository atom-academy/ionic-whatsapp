import { Component, OnInit } from '@angular/core';

import { CommonService } from 'src/app/services/common.service';

//import { FirebaseX } from "@ionic-native/firebase-x/ngx";
import { Platform, AlertController } from '@ionic/angular';

//import firebase from 'firebase';

import { AngularFireAuth } from '@angular/fire/auth';
import firebase, { User } from 'firebase/app';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  public recaptchaVerifier:firebase.auth.RecaptchaVerifier;

  area_code: string;
  phone_number: Number;
  sms_code: Number;

  constructor(
    private platform: Platform,
    private common: CommonService,
    public alertController: AlertController,
    private router: Router,
    private afAuth: AngularFireAuth,
    private usersService: UsersService
    ) {
    }

    
  ngAfterViewInit() {
    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
      'size': 'invisible',
      'callback': function (response) {
        console.log( 'recaptcha resolved auto' );
      // reCAPTCHA solved, allow signInWithPhoneNumber.
      }
      });
  }

  ngOnInit() {

    

  }

  savePhone() {
    
    this.afAuth.auth.signInWithPhoneNumber(this.area_code+this.phone_number, this.recaptchaVerifier).then(
      (data) => {
        console.log( 'success',data )
        this.askCode( data );
        
      },
      (err) => {
        console.log( 'error',err )
      }
    )
    /*
    const appVerifier = this.recaptchaVerifier;
    const phoneNumberString = "+" + this.phone_number;
    firebase.auth().signInWithPhoneNumber(phoneNumberString, appVerifier)
      .then( confirmationResult => {
        // SMS sent. Prompt user to type the code from the message, then sign the
        // user in with confirmationResult.confirm(code).
        this.askCode( confirmationResult );
      });
      */
  }

  async askCode( smsRequest: any) {
    const alert = await this.alertController.create({
      header: 'Verificación',
      message: 'Por favor ingresa el código que hemos enviado por SMS.',
      inputs: [{ name: 'confirmationCode', placeholder: 'Código SMS' }],
      buttons: [{
        text: 'Validar',
        handler: data => {
          console.log( data.confirmationCode );
          smsRequest.confirm( data.confirmationCode ).then((result) => {
            // User signed in successfully.
            console.log('signed in',result);
            this.usersService.addUser(result.user.uid,{
              displayName: result.user.displayName,
              photoURL: result.user.photoURL,
              email: result.user.email,
              emailVerified: result.user.emailVerified,
              phoneNumber: result.user.phoneNumber
            });
            this.router.navigate(['/home']);
            // ...
          }).catch(function (error) {
            // User couldn't sign in (bad verification code?)
            // ...
            console.error('error',error);
          });
        }
      }]
    });

    await alert.present();
  }

  /*
  async askCode( confirmationResult: any ) {
    let prompt = this.alertCtrl.create({
      header: 'Enter the Confirmation code',
      inputs: [{ name: 'confirmationCode', placeholder: 'Confirmation Code' }],
      buttons: [
        { text: 'Cancel',
          handler: data => { console.log('Cancel clicked'); }
        },
        { text: 'Send',
          handler: data => {
            confirmationResult.confirm(data.confirmationCode)
              .then(function (result) {
                // User signed in successfully.
                console.log('success', result);
                // ...
              }).catch(function (error) {
                // User couldn't sign in (bad verification code?)
                // ...
                console.error( 'error', error )
              });
          }
        }
      ]
    });
    (await prompt).present();
  }
*/


}
