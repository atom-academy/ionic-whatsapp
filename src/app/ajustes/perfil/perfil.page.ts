import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

import { AngularFireAuth } from '@angular/fire/auth';

import { AngularFireStorage } from '@angular/fire/storage';

import { Router } from '@angular/router';

import 'firebase/storage';

import { CommonService } from 'src/app/services/common.service';

import { ActionSheetController } from '@ionic/angular';

import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  user: any;

  profilePicture: any;

  options: CameraOptions = {
    quality: 70,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    correctOrientation: true,
    sourceType: 1
  }

  constructor(
    private camera: Camera,
    private router: Router,
    private afAuth: AngularFireAuth,
    private storage: AngularFireStorage,
    private commonService: CommonService,
    public actionSheetController: ActionSheetController,
    private usersService: UsersService
  ) {
    this.user = this.afAuth.auth.currentUser
  }
  

  ngOnInit() {
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Seleccionar avatar',
      buttons: [{
        text: 'Desde la cámara',
        icon: 'camera',
        handler: () => {
          this.options.sourceType = 1;
          this.takePicture();
        }
      }, {
        text: 'Desde la galería',
        icon: 'images',
        handler: () => {
          this.options.sourceType = 0;
          this.takePicture();
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  changeName() {
    console.log('enter press');
    
  
    //this.nameInput['_native'].nativeElement.blur();
  }

  takePicture() {
    
    this.camera.getPicture(this.options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      
      //this.profilePicture = 'data:image/jpeg;base64,' + imageData;
        this.commonService.presentLoading();
        this.uploadFile( this.user.uid+'.jpg', 'data:image/jpeg;base64,' + imageData ).then(
          ( result ) => {
            // Aquí hay URL
            this.user.updateProfile({
              photoURL: result
            }).then(
              () => {
                this.usersService.updateUser(this.user.uid,{photoURL: result});
                this.profilePicture = result;
                this.commonService.dismissLoading();
                this.commonService.msg('El avatar ha sido actualizado');
              }
            ).catch(
              ( err ) => {
                this.commonService.dismissLoading();
                this.commonService.msg('Ha ocurrido un error al subir la foto');
              }
            )
          }
        ).catch(
          ( err ) => {
            console.error( err );
          }
        );
    
      
      
      
     }, (err) => {
      // Handle error
      console.error( err );
     });
  }

  uploadFile( filename: string, base64: string ) {
    return new Promise( (resolve,reject) => {
      
      const filePath = 'avatars/'+filename;
      const ref = this.storage.ref(filePath);
      ref.putString(base64, 'data_url').then(
        (data) => {
          data.ref.getDownloadURL().then(
            ( url ) => {
              resolve( url );
            }
          ).catch(
            ( err ) => {
              reject( err );
            }
          );
        }
      ).catch(
        (err) => {
          reject ( err )
        }
      );
      /*
      const task = ref.putString(base64, 'data_url').then(
        (data) => {
          
          data.ref.getDownloadURL().then(
            (url) => {
              resolve( url );
            }
          );
        },
        (error => {
          reject( error );
          console.log( 'error upload', error);
        })
      )
      */
    } );
  }

  

  closeSession() {
    this.afAuth.auth.signOut().then(
      () => {
        this.router.navigate(['/register']);
      },
      () => {
        alert('Error closing session')
      }
    )
  }

  updateUser( field: string, ev: any ) {

    var data = {};
    data[field] = ev.target.value;
    
    this.usersService.updateUser(this.user.uid,data).then(
      () => {
        console.log('La información ha sido actualizada');
        this.commonService.msg('La información ha sido actualizada');
      }
    ).catch(
      () => {
        console.log('Ha ocurrido un error al actualizar la información');
        this.commonService.msg('Error al guardar la información');
      }
    );
    /*
    console.log('Send to firebase', ev.target.value);
    this.user.updateProfile({
      displayStatus: ev.target.value
    }).then(function() {
      // Update successful.
      console.log('profile updated')
    }).catch(function(error) {
      // An error happened.
      console.log('profile updated error')
    });
    */
  }

}
