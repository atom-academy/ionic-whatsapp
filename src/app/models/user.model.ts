export class User {
    uid: string;
    phoneNumber: string;
    displayName: string;
    photoURL: string = 'assets/default-avatar.png';
    estado: string;
    last_connection: Date;

    constructor( userData?: any ) {
        if ( userData ) {
            Object.keys( userData ).forEach( key => {
                if ( userData[key] ) {
                    this[key] = userData[key];
                }
            });
        }
    }

}