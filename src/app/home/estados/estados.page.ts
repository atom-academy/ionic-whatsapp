import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-estados',
  templateUrl: './estados.page.html',
  styleUrls: ['./estados.page.scss'],
})
export class EstadosPage implements OnInit {

  estados = [
    {
      avatar: 'https://i.pravatar.cc/150',
      name: 'Fernando Quiroz',
      last: '2020-02-08 14:00:04'
    },
    {
      avatar: 'https://i.pravatar.cc/151',
      name: 'Raúl Díaz',
      last: '2020-02-08 13:50:04'
    },
    {
      avatar: 'https://i.pravatar.cc/152',
      name: 'Rebeca Solís',
      last: '2020-02-08 13:27:04'
    },
    {
      avatar: 'https://i.pravatar.cc/153',
      name: 'Nancy Díaz',
      last: '2020-02-08 12:58:04'
    },
    {
      avatar: 'https://i.pravatar.cc/154',
      name: 'Carlos González',
      last: '2020-02-08 11:09:04'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
