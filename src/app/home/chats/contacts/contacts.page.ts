import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Contacts, Contact } from '@ionic-native/contacts';
import { File } from '@ionic-native/file/ngx';
import { CommonService } from 'src/app/services/common.service';
import { IonInput } from '@ionic/angular';
import { FirebaseService } from '../../../services/firebase.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { UsersService } from 'src/app/services/users.service';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.page.html',
  styleUrls: ['./contacts.page.scss'],
})
export class ContactsPage implements OnInit {

  @ViewChild('searchInput', {static: false}) searchInput: IonInput;

  search: boolean = false;

  /*
  contacts = [
    {
      avatar: 'https://i.pravatar.cc/150',
      name: 'Fernando Quiroz',
      status: 'No molestar'
    },
    {
      avatar: 'https://i.pravatar.cc/151',
      name: 'Raúl Díaz',
      status: 'Disponible'
    },
    {
      avatar: 'https://i.pravatar.cc/152',
      name: 'Rebeca Solís',
      status: 'Disponible'
    }
  ];
  */

  contacts: Contact[];

  filteredContacts: Contact[];

  user: any;

  constructor(
    private nativeContacts: Contacts,
    private common: CommonService,
    private userServices: UsersService,
    private chatServices: ChatService
  ) {
    
    
  }

  ngOnInit() {
    this.common.presentLoading();
    this.nativeContacts.find( ['displayName','name','photos'] ,{filter: "", multiple: true, hasPhoneNumber:true	}).then(
      (data) => {
        this.contacts = data;
        this.filteredContacts = this.contacts;
        setTimeout( () => {
          this.common.dismissLoading();
        }, 1500);
      },
      ( err ) => {
        console.log( err )
      }
    )
    
  }

  showSearch() {
    this.search = true;
    
    this.searchInput.setFocus();

  }

  closeSearch() {
    this.search = false;
    this.filteredContacts = this.contacts;
  }

  filterSearch( ev: any ) {
    var searchString = ev.target.value.toLowerCase();
    this.filteredContacts = this.contacts.filter( el => el.displayName.toLowerCase().indexOf( searchString ) != -1 );
    console.log(this.filteredContacts);
  }

  addChat(contact: any) {
    this.userServices.getUserByPhone(contact.phoneNumbers[0].value).subscribe(
      (userSnap) => {
        if ( userSnap.length > 0 ) {
          var userID = userSnap[0].payload.doc.id;
          var userInfo = userSnap[0].payload.doc.data();

          this.chatServices.addChatListItem( this.common.user.uid, userID, userInfo );
          console.log('Item Updated')
          this.common.navForward('/home/chats/details/'+userID)
        } else {
          this.common.msg('El usuario no está registrado en Whatsapp')
        }
      }
    );
    // this.fbs.createDocumentChat(this.user,contact.uid);
  }

}
