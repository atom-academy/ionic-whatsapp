import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { ChatService } from 'src/app/services/chat.service';
import { UsersService } from 'src/app/services/users.service';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  destinatario: string;
  origin: string;

  user: User;
  newMessage: string = '';

  messages: any[] = [];

  constructor(
    private route: ActivatedRoute,
    private common: CommonService,
    private chatService: ChatService,
    private userService: UsersService
  ) {
    this.destinatario = this.route.snapshot.paramMap.get('userId');
    this.origin = this.common.user.uid;

    this.userService.getUserById( this.destinatario ).subscribe(
      user => {
        this.user = new User ( user.payload.data() );
      }
    );
  }

  ngOnInit() {
    this.chatService.getChatMessages(this.origin, this.destinatario).subscribe(
      msg => {
        console.log('mensage recibido', msg.map( data => data.payload.doc.data() ) );
        if ( this.messages.length == 0 ) {
          this.messages = msg.map( m => {
            return m.payload.doc.data();
          });
        } else {
          this.messages.push( msg[msg.length-1].payload.doc.data() );
        }
        
      }
    )
  }

  sendMessage() {
    console.log('origin', this.origin);
    console.log('destinatario', this.destinatario);
    console.log('enviar', this.newMessage);
    this.chatService.addChatMessage(this.origin, this.destinatario, this.newMessage );
  }

}
