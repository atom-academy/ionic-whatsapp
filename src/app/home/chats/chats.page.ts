import { Component, OnInit } from '@angular/core';

import { CommonService } from 'src/app/services/common.service';
import { UsersService } from 'src/app/services/users.service';

import { ChatService } from 'src/app/services/chat.service';

import { Chat } from 'src/app/models/chat.model';


@Component({
  selector: 'app-chats',
  templateUrl: './chats.page.html',
  styleUrls: ['./chats.page.scss'],
})
export class ChatsPage implements OnInit {

  recentChats: Chat[] = [];

  constructor(
    private usersService: UsersService,
    private chatService: ChatService,
    private common: CommonService
  ) { }

  ngOnInit() {
    this.chatService.getChatsList( this.common.user.uid ).subscribe(
      (chatSnap) => {
        this.recentChats = [];
        chatSnap.forEach( chatData => {
          var item = chatData.payload.doc.data();
          item.id = chatData.payload.doc.id;
          this.recentChats.push( new Chat ( item ) );
        })
        
      }
    );

    /*
    this.usersService.getUsers().subscribe(
      (userSnap) => {
        userSnap.forEach( userData => {
          console.log( userData.payload.doc.data() );
        })
      }
    );
    /*
    this.firestoreService.getChats().subscribe((chatsSnapshot) => {
      
      chatsSnapshot.forEach((catData: any) => {
        console.log( catData.payload.doc.data() )
      })
    });
    */
  }



  goToContacts() {
    this.common.navRoot('/home/chats/contacts')
  }

}
