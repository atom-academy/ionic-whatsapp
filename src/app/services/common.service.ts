import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { AngularFireAuth } from "@angular/fire/auth";
import { User } from "src/app/models/user.model";
import { resolve } from 'url';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  loading: any;

  user: User;

  constructor(
    public nativeStorage: NativeStorage,
    private navController: NavController,
    private toastController: ToastController,
    public loadingCtrl: LoadingController,
    private AFauth : AngularFireAuth
  ) {
    console.log('inicializado servicio common');
    this.AFauth.user.subscribe( userUpdated => {
      this.user = new User({
        uid: userUpdated.uid,
        displayName: userUpdated.displayName,
        phoneNumber: userUpdated.phoneNumber,
        photoURL: userUpdated.photoURL
      });
    });
  }

  

  setItem(key: string, value: any){
    return new Promise( (resolve) => {

      this.nativeStorage.setItem(key,value).then(
        ( data ) => {
          resolve ( data );
        },
        () => {
          resolve ( );
        }
      )

    } )    
    
  }


  getItem(key: string) {
    return new Promise ( (resolve) => {
      this.nativeStorage.getItem(key).then(
        ( data ) => {
          resolve ( data );
        },
        () => {
          resolve ( )
        }
      )
    } );
  }

  navRoot( page: string ) {
    this.navController.navigateRoot( page );
  }

  navForward( page: string ) {
    this.navController.navigateForward( page );
  }

  async msg( text: string, opt?: any ) {
    const toast = await this.toastController.create({
      message: text,
      duration: (opt && opt.duration) ? opt.duration : 2000,
      position: (opt && opt.position) ? opt.position : 'top'
    });
    toast.present();
  }

  async presentLoading(message?: string) {
    this.loading = await this.loadingCtrl.create({
      message: message
    });
    return await this.loading.present();
  }

  dismissLoading() {
    if ( this.loading ) {
      this.loading.dismiss()
    }
  }

}
